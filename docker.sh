#!/bin/sh
#
# This is free software, licensed under the GNU General Public License v3.
# See /LICENSE for more information.
#
clear

echo "
     ██╗██████╗     ██████╗  ██████╗  ██████╗██╗  ██╗███████╗██████╗
     ██║██╔══██╗    ██╔══██╗██╔═══██╗██╔════╝██║ ██╔╝██╔════╝██╔══██╗
     ██║██║  ██║    ██║  ██║██║   ██║██║     █████╔╝ █████╗  ██████╔╝
██   ██║██║  ██║    ██║  ██║██║   ██║██║     ██╔═██╗ ██╔══╝  ██╔══██╗
╚█████╔╝██████╔╝    ██████╔╝╚██████╔╝╚██████╗██║  ██╗███████╗██║  ██║
 ╚════╝ ╚═════╝     ╚═════╝  ╚═════╝  ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝

                     ==== Create by 沙拉野人 ====
              本脚本由沙拉野人免费分享，感谢你的支持！
              Server文件有类似后门功能,无需登录即可添加Cookie 介意勿用。
"

docker_install() {
	echo "检查Docker......"
	if [ -x "$(command -v docker)" ]; then
		if [ -x "$(command -v docker-compose)" ]; then
			echo "检查到Docker已安装!"
		fi
	else
		if [ -r /etc/os-release ]; then
			lsb_dist="$(. /etc/os-release && echo "$ID")"
		fi
		if [ $lsb_dist == "openwrt" ]; then
			echo "openwrt 环境请自行安装docker"
			#exit 1
		else
			echo "安装docker环境..."
			curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
			echo "安装docker环境...安装完成!"
			echo "安装docker-compose环境..."
			wget https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m) -O /usr/local/bin/docker-compose
			echo "安装docker-compose环境...安装完成!"
			sudo chmod +x /usr/local/bin/docker-compose
			sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
			docker-compose --version
			systemctl enable docker
			systemctl start docker
		fi
	fi
}

docker_install

docker ps
